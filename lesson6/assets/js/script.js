


const products = [
    {
        name: 'Флюїд для обличчя для сухої шкіри',
        image: './assets/img/katalog1.png',
        price: '630 грн',
        availability: 'в наявності',
    },
    {
        name: 'Антиоксидантний крем для обличчя',
        image: './assets/img/katalog2.png',
        price: '850 грн',
        availability: 'в наявності',
    },
    {
        name: 'Гель для вмивання для проблемної шкіри',
        image: './assets/img/katalog3.png',
        price: '590 грн',
        availability: 'в наявності',
    },
    {
        name: 'Зволожуюча сироватка для нормальної шкіри',
        image: './assets/img/katalog4.png',
        price: '480 грн',
        availability: 'в наявності',
    },
    {
        name: 'Зволожуючий крем для сухої шкіри',
        image: './assets/img/katalog5.png',
        price: '830 грн',
        availability: 'в наявності',
    },
    {
        name: 'Тонік для обличчя для нормальної шкіри',
        image: './assets/img/katalog6.png',
        price: '450грн',
        availability: 'в наявності',
    },
    {
        name: 'Пінка для очищення шкіри обличчя',
        image: './assets/img/katalog7.png',
        price: '560 грн',
        availability: 'в наявності',
    },
    {
        name: 'Набір Денний та нічний крем',
        image: './assets/img/katalog8.png',
        price: '1230 грн',
        availability: 'в наявності',
    },
    {
        name: 'Матуючий денний крем для жиної шкіри',
        image: './assets/img/katalog9.png',
        price: '680 грн',
        availability: 'в наявності',
    },
    {
        name: 'Нічний антивіковий крем  для сухої шкіри',
        image: './assets/img/katalog10.png',
        price: '860 грн',
        availability: 'в наявності',
    },
    {
        name: 'Гель для зняття макіяжу',
        image: './assets/img/katalog11.png',
        price: '730 грн',
        availability: 'в наявності',
    },
    {
        name: 'Антивікова сироватка  для зони навколо очей',
        image: './assets/img/katalog12.png',
        price: '900 грн',
        availability: 'в наявності',
    },
    {
        name: 'Лосьон для нормальної та комбінованої шкіри',
        image: './assets/img/katalog12.png',
        price: '1050 грн',
        availability: 'в наявності',
    },
    {
        name: 'Скраб для чутливої шкіри',
        image: './assets/img/katalog13.png',
        price: '890 грн',
        availability: 'в наявності',
    },
    {
        name: 'Гель для глубокого очищення шкіри обличчя',
        image: './assets/img/katalog5.png',
        price: '890 грн',
        availability: 'в наявності',
    },

];


products.forEach(product => {
    cardItem(product)
});

function cardItem(product) {
    let productCard = document.querySelector('.product-card')

        let productItem = document.createElement('div');
        productItem.className = 'product-item';
        
        let productLink = document.createElement('a');
        productLink.className = 'product-link';
        
        let productImg = document.createElement('img');
        productImg.className = 'product-item-img'
        productImg.src = product.image
        
        let productName = document.createElement('div');
        productName.className = 'product-name';
        productName.innerText = product.name
        
        let productPrice = document.createElement('div');
        productPrice.className = 'product-price';
        productPrice.innerText = product.price
        
        let availability = document.createElement('p');
        availability.className ='availability';
        availability.innerText = product.availability
        
        
        let moreLink = document.createElement('div')
        moreLink.className = 'more-link';
        let cardLink = document.querySelector('a')
        cardLink.className = 'card-link';
        
        let btnWrapp = document.createElement('div');
        btnWrapp.className = 'btn-wrapp-in-bascket';
        
        let catalogBtn =  document.createElement('div');
        catalogBtn.className = 'catalog-btn-in-bascket';
        catalogBtn.innerText = 'В кошик'
        
        productCard.appendChild(productItem);
        productItem.appendChild(productLink);
        productLink.appendChild(productImg);
        productLink.appendChild(productName);
        productLink.appendChild(productPrice);
        productItem.appendChild(moreLink);
        moreLink.appendChild(availability);
        moreLink.appendChild(btnWrapp);
        btnWrapp.appendChild(catalogBtn);
}





