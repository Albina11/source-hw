const slider = document.querySelector('.slider');
const slides = document.querySelectorAll('.slide');
const prevArrow = document.querySelector('.prev');
const nextArrow = document.querySelector('.next');
const paginationDots = document.querySelectorAll('.dot');
let currentSlide = 0;

initSlider();

function initSlider() {
  slides[currentSlide].classList.add('active');
  paginationDots[currentSlide].classList.add('active');

  prevArrow.addEventListener('click', function() {
    if(currentSlide -1 in slides) {
      slides[currentSlide].classList.remove('active');
      paginationDots[currentSlide].classList.remove('active');
      currentSlide = currentSlide - 1
      slides[currentSlide].classList.add('active');
      paginationDots[currentSlide].classList.add('active');
    }
  });

  nextArrow.addEventListener('click', function() {
    if(currentSlide + 1 in slides) {
      slides[currentSlide].classList.remove('active'); 
      paginationDots[currentSlide].classList.remove('active');
      currentSlide = currentSlide + 1
      slides[currentSlide].classList.add('active');
      paginationDots[currentSlide].classList.add('active');
    }
}
    )
}

paginationDots.forEach(function(dot, index) {
    dot.addEventListener('click', function() {
      slides[currentSlide].classList.remove('active');
      paginationDots[currentSlide].classList.remove('active');
      currentSlide = index;
      slides[currentSlide].classList.add('active');
      paginationDots[currentSlide].classList.add('active');
    });
  });