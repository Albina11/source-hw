

$(document).ready(function() {
    
    $('.addSkill').click(function(e) {
      e.preventDefault(); 
      let lastSkill = $('.skills input:last');
      let newSkill = '<input type="text" name="skill[]" placeholder="Введіть навичку"><a href="#" class="removeSkill">-</a>';
      lastSkill.after(newSkill);
    });
    
   
    $('form').on('click', '.removeSkill', function(e) {
      e.preventDefault(); 
      $(this).prev('input').remove();
      $(this).remove();
    });
  });
  
  