// 1) Необхідно реалізувати функцію, що друкує всі прості числа із заданого діапазону.

function simpleNumbers(start, end) {

  let simpleNumbers = [];
  for(let index = start; index <= end; index++) {

      let arr = [];

      for(let subIndex = index; subIndex >= 1; subIndex--) {
          if(index%subIndex === 0 ) {
              arr.push(index)
          }
      }

      if(arr.length === 2) {
        simpleNumbers.push(index);
      }
  }

  return simpleNumbers;
}

simpleNumbers(6, 17);
console.log(simpleNumbers(6, 17));
 



// 2) Знайти суму цифр заданого числа.
function sum(a) {
    let result = 0;
    for (let i = 0; i <= a; i++) {
      result +=i;
    }
    
    return result;
  };

console.log(sum(10));

// 3) Дано натуральні числа n і m. Написати функцію, яка повертає результат операції додавання двох чисел. 
// Перше утворено з k молодших цифр числа n, друге - з k старших цифр числа m.

function summ(n, m) {

  let kn = String(n).split("");
  let km =  String(m).split("");

  km = Number(km[0]);
  kn = Number(kn[kn.length-1]);

  return km * kn;
  
}

summ(1445, 84523);
