// - Реалізуємо функції валідації:
// - телефон;
// - email;
// - ПІБ.
function validatePhoneNumber(phoneNumber) {
  const phoneNumberRegex = /^\+\d{2} \d{3} \d{3} \d{2} \d{2}$/;
  return  /^\d+$/.test(phoneNumber) || phoneNumberRegex.test(phoneNumber);
}
  
  
console.log(validatePhoneNumber('30964454036'));

  function validateEmail(email) {
    const emailRegex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    return emailRegex.test(email);
  }
  
  function validateFullName(fullName) {
    const fullNameRegex = /^[a-zA-Z]+ [a-zA-Z]+$/;
    return fullNameRegex.test(fullName);
  }

// Реалізуємо функцію підрахунку грошей, що видаються
// банкоматом. Користувач запитує суму, а банкомат має
// підібрати варіант із найбільшими купюрами. Врахувати, що
// кількість банкнот обмежена.


function atm(amount) {

  const banknotes = {
      100 : 3,
      200 : 3,
      500 : 3,
  }

  const cash = Array();
  const ordered_banknotes = Object.keys(banknotes).sort().reverse()

  for(let index in ordered_banknotes) {

      do {
          if (amount >= ordered_banknotes[index] && banknotes[ordered_banknotes[index]] > 0) {
            
              amount -= ordered_banknotes[index]
              banknotes[ordered_banknotes[index]]--;
              cash.push(ordered_banknotes[index])
          }

      } while(amount >= ordered_banknotes[index] && banknotes[ordered_banknotes[index]] > 0)
  }

  // console.log(banknotes)
  // console.log(amount)

  if(amount !== 0) {
      return 'We haven\'t enough money'
  }

  return cash
}


console.log(atm(400));
console.log(atm(300));

