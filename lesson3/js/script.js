// 1)Користувач вводить 10 чисел. Знайти максимальне число.
let numbers = [1,2, -2, 3, 74];

function maxValue(numbers) {
    let maxValue = null;

    if(Array.isArray(numbers)) {
        for(let index  in numbers) {
            if(maxValue < numbers[index]) {
                maxValue = numbers[index];
            }
        }
    }

    return maxValue;
}

maxValue(numbers);


// 2) Спортсмен пробігає за 1-й день М км, кожного  наступного дня він збільшує норму пробігу на К%.
// Визначте через скільки днів норма пробігу може стати більше 50 км
function speedRunner(kmDay, stepInDay) {
    let days = 1;

    do {
        days++;
        kmDay = kmDay + (kmDay/100)*stepInDay;
    } while (kmDay <= 50)

    return days
}

console.log(speedRunner(1,50))



// 3) Виведення таблиці множення Піфагора


function pifagors() {
    let counter = 10;
    let arr = [];

    let table = document.getElementById('table');

    for(let row = 1; row <= counter; row++) {
        arr = []

        let tr = document.createElement('tr');

        for(let col = 1; col <= counter; col++) {
            let td = document.createElement('td');

            if(col === 1 || row === 1) {
                td.style.backgroundColor = 'green';
            }

            if(row === col) {
                td.style.backgroundColor = 'pink';
            }

            td.innerText = row * col;
            tr.append(td);

            arr.push(row * col);
        }

        table.append(tr);
    }
}

console.log(pifagors());
